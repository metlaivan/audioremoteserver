/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.audioremote.data;

import java.io.Serializable;

/**
 * Class for exchange data client server Send url necessarily
 *
 * @author Ivan Metala
 */
public class ExchangeData implements Serializable {

    private static final long serialVersionUID = 1L;
    private String url;
    private double moveTo;
    private double volume;
    private double moveto;

    public enum Command {

        START, STOP, PAUSE, MOVETO, MUTE, SETVOLUME, GETPLAYLIST, DISPOSE,REPEAT
    };
    private Command command;

    public ExchangeData() {
        this.command = Command.START;
        this.moveTo = 0.0;
        this.url = "";
        this.volume = 0.0;
    }

    public ExchangeData(Command command, double volume) {
        this.volume = volume;
        this.command = command;
    }

    public ExchangeData(Command command, String url, double volume) {
        this.url = url;
        this.volume = volume;
        this.command = command;
    }

    public ExchangeData(Command command, double moveto, String url) {
        this.url = url;
        this.moveto = moveto;
        this.command = command;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Command getCom() {
        return command;
    }

    public void setCom(Command com) {
        this.command = com;
    }

    public double getMoveTo() {
        return moveTo;
    }

    public void setMoveTo(double moveTo) {
        this.moveTo = moveTo;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }
}
