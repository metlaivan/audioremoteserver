/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.audioremote.data;

/**
 * Definition OS
 * @author Ivan Metla
 */
public class OsDefinition {

    public static boolean isWindows() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("win") >= 0);
    }

    public static boolean isMac() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("mac") >= 0);
    }

    public static boolean isUnix() {
        String os = System.getProperty("os.name").toLowerCase();        //linux or unix
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
    }

    public static String getOSVerion() {
        String os = System.getProperty("os.version");
        return os;
    }
}
