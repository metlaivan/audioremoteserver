package ua.cn.audioremote.server;

import com.perm.kate.api.Audio;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.ServerSocket;

import java.net.SocketTimeoutException;
import java.security.KeyManagementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ua.cn.audioremote.data.ExchangeData;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.net.ServerSocketFactory;
import static ua.cn.audioremote.data.ExchangeData.Command.DISPOSE;
import static ua.cn.audioremote.data.ExchangeData.Command.MOVETO;

/**
 * Server class of application
 *
 * @author ivan
 */
public class SocketServer extends Thread {
    
    private ServerSocket serverSocket;
    private  SSLSocket server ;
    private Player player = null;
    private final static char KS_PASS[] = "q1w2e3".toCharArray();
    private final static String KS_PATH = "/ua/cn/audioremote/data/AudioRemouteServer"; // myself
    private String pathTom3u ;
    private  ArrayList<Audio> listAudioInfo;
    
    public SocketServer(int port,String path)throws IOException{
        this.pathTom3u=path;
         new Thread(new Runnable()
        {
            public void run() //Этот метод будет выполняться в побочном потоке
            {                
                listAudioInfo=getPlayList(pathTom3u);                
            }
        }).start();
        
        KeyStore ks;
        try {            
            System.out.println("Init server");
            ks = KeyStore.getInstance("JKS");
            ks.load(getClass().getResourceAsStream(KS_PATH), KS_PASS);
            KeyManagerFactory kmf =
                    KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, KS_PASS);
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(kmf.getKeyManagers(), null, null);
            ServerSocketFactory ssf = sslcontext.getServerSocketFactory();
            serverSocket = (SSLServerSocket) ssf.createServerSocket(port);
           
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("FileNotFoundException");
        }  catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("There is no algorithm in ks.load");
        } catch (CertificateException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("kmf.init() no key");
        } catch (KeyManagementException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("sslcontext.init keymanagementexception");
        } catch (KeyStoreException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Could not get key store");
        }
    }

    @Override
    public void interrupt() {
        try {
            super.interrupt(); //To change body of generated methods, choose Tools | Templates.
            //server.close();
            if(serverSocket!=null){
            serverSocket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run() {
        Player pl = null;
        System.out.println("Start AudioServer on " + serverSocket.getLocalPort() + "...");  
        while (true) {
            try {
                System.out.println("Waiting client");
                server = (SSLSocket) serverSocket.accept();                
                System.out.println("Just connected to " + server.getRemoteSocketAddress());
                ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
                Object o = ois.readObject();
                if (o instanceof ExchangeData) {
                    ExchangeData ed = (ExchangeData) o;
                    Object retO = selectActionPlayler(ed);
                    if (retO != null) {
                        ObjectOutputStream ous = new ObjectOutputStream(server.getOutputStream());
                        ous.writeObject(retO);
                    }
                } else {
                    System.out.println("Else informatoin");
                }
                ois.close();
                server.close();
            } catch (SocketTimeoutException s) {
                System.out.println("Socket timed out!");
                break;
            } catch (IOException e) {
                System.out.println("Socket IOException!");                
            } catch (ClassNotFoundException ex) {
                
                Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param ed data
     */
    private Object selectActionPlayler(ExchangeData ed) {
        switch (ed.getCom()) {
            case REPEAT:
                 if (player != null) {              
                        player.dispose();
                        player = new Player(ed.getUrl());
                        player.start();
                    
                } else {
                    player = new Player(ed.getUrl());
                    player.start();
                }
                break;
            case START:
                if (player != null) {
                    if (player.getMEDIA_URL().equals(ed.getUrl())) {
                        player.start();
                    } else {
                        player.dispose();
                        player = new Player(ed.getUrl());
                        player.start();
                    }
                } else {
                    player = new Player(ed.getUrl());
                    player.start();
                }
                break;
            case STOP:
                if (player != null) {
                    player.stop();
                }
                break;
            case PAUSE:
                if (player != null) {
                    player.pause();
                }
                break;
            case DISPOSE:
                if (player != null) {
                    player.dispose();
                }
                break;
            case MOVETO:
                if (player != null) {
                    player.moveTo(ed.getMoveTo());
                }
                break;
                 case SETVOLUME:
                if (player != null) {
                    player.setVolume(ed.getVolume());
                }
                break;
            case MUTE:
                if (player != null) {
                    player.mute();
                }
                break;
            case GETPLAYLIST:      
                return listAudioInfo;
            
        }
        return null;
    }

    /**
     *
     * @param Path path to file of plalist *.m3u
     * @return LinkedList<AudioInfo>
     */
    public ArrayList<Audio> getPlayList(String Path) {
        System.out.println("Start Parser PlayList");
        try {
            M3U_Parser m3uP = new M3U_Parser(Path);
            LinkedList<String> listUrl = m3uP.getUrl();
            ArrayList<Audio> listAudioInfo = new ArrayList<>();
            for (String string : listUrl) {
                if(new File(string).exists())
                listAudioInfo.add(new Audio(string));
            }
            System.out.println("Finish Parser PlayList");
            return listAudioInfo;
        } catch (Exception ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }

    public ArrayList<Audio> getListAudioInfo() {
        return listAudioInfo;
    }

    public void setListAudioInfo(ArrayList<Audio> listAudioInfo) {
        this.listAudioInfo = listAudioInfo;
    }

    public String getPathTom3u() {
        return pathTom3u;
    }

    public void setPathTom3u(String pathTom3u) {
        this.pathTom3u = pathTom3u;
    }
    
}
