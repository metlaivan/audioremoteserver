package ua.cn.audioremote.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * This class plays audio from url
 *
 * @author Ivan Metla
 */
public class Player {

    private String MEDIA_URL = "";
    private Media media = null;
    private MediaPlayer mediaPlayer = null;
    private boolean mute = true;

    /**
     * Allocates a new Player object
     *
     * @param URL url for playing audio example "file:///home/ivan/1.mp3"
     */
    public Player(String URL) {
       
            MEDIA_URL =URL;
            initJavaFX();
          // System.out.println(MEDIA_URL);
           // media = new Media(URLEncoder.encode(MEDIA_URL, "UTF-8"));
             media = new Media(MEDIA_URL);
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.setVolume(0.5);
           // System.out.println(media.getMetadata().values().toString());

       
    }
/**
 * Init javafx from swing application
 */
    private void initJavaFX() {
        final CountDownLatch latch = new CountDownLatch(1);

        new Thread(new Runnable() {
            @Override
            public void run() {
                @SuppressWarnings("unused")
                JFXPanel p = new JFXPanel();
                latch.countDown();
            }
        }, "JavaFX-initializer").start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start play
     */
    public void start() {
        mediaPlayer.play();
        //media.getMetadata().g
    }

    /**
     * Dispose player
     */
    public void dispose() {
        mediaPlayer.dispose();
    }

    /**
     * Stop player
     */
    public void stop() {
        mediaPlayer.stop();
    }

    /**
     * Pause player
     */
    public void pause() {
        mediaPlayer.pause();
    }

    /**
     * Move to time
     *
     * @param time time in seconds
     */
    public void moveTo(Double time) {
        mediaPlayer.seek(new Duration(time * 1000)); 
    }

    /**
     * Set mute
     */
    public void mute() {
        mediaPlayer.setMute(mute);
        mute = !mute;
    }

    /**
     * Set volume
     *
     * @param vl volume from 0.0 to 1.0
     */
    public void setVolume(double vl) {
        mediaPlayer.setVolume(vl);
    }

    public String getMEDIA_URL() {
        return MEDIA_URL;
    }

    public void setMEDIA_URL(String MEDIA_URL) {
        this.MEDIA_URL = MEDIA_URL;
    }
    /**
     * testing method
     * @param args 
     */
     public static void main(String[] args) {
         new Player("file:///home/ivan/1.mp3").start();
     }
}
