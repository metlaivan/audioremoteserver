/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.cn.audioremote.server;

import com.perm.kate.api.Audio;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.BindException;
import java.util.Properties;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

/**
 *
 * @author ivan
 */
public class AudioRemoteServer extends javax.swing.JFrame {

    private SystemTray systemTray = SystemTray.getSystemTray();
    private TrayIcon trayIcon;
    private BufferedImage imageIcon;
    private boolean viseble = true;
    private final JFileChooser fc = new JFileChooser();
    private Properties prop = new Properties();
    private SocketServer ss;
    private java.util.Timer timer = new java.util.Timer(true);
    private String currentLanguage = "language";

    /**
     * Creates new form NewJFrame
     */
    public AudioRemoteServer() {
        String language = System.getProperty("user.language");
        switch (language) {
            case "en":
                currentLanguage += "-en";
                break;
            case "ru":
                currentLanguage += "-ru";
                break;
        }
        initComponents();        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        try {
            imageIcon = ImageIO.read(getClass().getResourceAsStream("ic_launcher.png"));
        } catch (IOException ex) {
            Logger.getLogger(AudioRemoteServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        trayIcon = new TrayIcon(imageIcon, "AudioRemoteServer");
        trayIcon.setImageAutoSize(true);
        setIconImage(imageIcon);
        trayIcon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.out.println("clicOnTray");
                setVisible(viseble);
                viseble = !viseble;
            }
        });
        PopupMenu popupMenu = new PopupMenu();
        MenuItem itemOptions = new MenuItem(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("options"));
        itemOptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viseble) {
                    setVisible(viseble);
                    viseble = !viseble;
                    jTabbedPane1.setSelectedIndex(0);
                }

            }
        });
        popupMenu.add(itemOptions);
        MenuItem itemAbout = new MenuItem(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("about"));
        itemAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viseble) {
                    setVisible(viseble);
                    viseble = !viseble;
                    jTabbedPane1.setSelectedIndex(1);
                }
            }
        });
        popupMenu.add(itemAbout);
        MenuItem itemExit = new MenuItem(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("exit"));
        itemExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ss != null) {
                    ss.interrupt();
                }
                dispose();
                System.exit(0);
            }
        });
        popupMenu.add(itemExit);
        trayIcon.setPopupMenu(popupMenu);
        try {
            systemTray.add(trayIcon);
        } catch (AWTException ex) {
            Logger.getLogger(AudioRemoteServer.class.getName()).log(Level.SEVERE, null, ex);
        }

        startServer();
    }

    private void startServer() {
        try {
            try {
                prop.load(new FileInputStream("config.properties"));
                jTFPort.setText(prop.getProperty("port"));
                jTFPath.setText(prop.getProperty("pathm3u"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AudioRemoteServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AudioRemoteServer.class.getName()).log(Level.SEVERE, null, ex);
            }

            ss = new SocketServer(Integer.parseInt(prop.getProperty("port")), prop.getProperty("pathm3u"));
            ss.start();
        } catch (IOException ex) {
            timer.schedule(new MessageDisplayTask(trayIcon, "AudioRemoteServer", "Change server port to another", TrayIcon.MessageType.ERROR), 5000);
            //  trayIcon.displayMessage("AudioRemoteServer", "Change server port to another", TrayIcon.MessageType.ERROR);
        }


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLPort = new javax.swing.JLabel();
        jTFPort = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTFPath = new javax.swing.JTextField();
        jBOpen = new javax.swing.JButton();
        jBSave = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();

        setTitle("AudioRemoteServer");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jTabbedPane1.setToolTipText("");

        jLPort.setText(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("port"));

        jTFPort.setText("5454");
        jTFPort.setToolTipText("");
        jTFPort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTFPortActionPerformed(evt);
            }
        });

        jLabel1.setText(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("path"));

        jBOpen.setText(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("open"));
        jBOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBOpenActionPerformed(evt);
            }
        });

        jBSave.setText(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("save"));
        jBSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jTFPath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBOpen))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTFPort, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLPort)
                            .addComponent(jLabel1))
                        .addGap(0, 218, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLPort)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTFPort, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFPath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 124, Short.MAX_VALUE)
                .addComponent(jBSave)
                .addContainerGap())
        );

        jTabbedPane1.addTab(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("options"), jPanel1);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("AudioRemote server");

        jTextPane1.setEditable(false);
        jTextPane1.setText("It is a server for AudioRemote. AudioRemote is an application for remote broadcasting audio from vk.com and custom playlist. \n\naudioremote.pp.ua");
        jScrollPane1.setViewportView(jTextPane1);

        jLabel3.setText("Developer:");

        jTextPane2.setEditable(false);
        jTextPane2.setText("Metla Ivan\nemail: metlaivan@audioremote.pp.ua");
        jScrollPane2.setViewportView(jTextPane2);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(1, 1, 1)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(59, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(java.util.ResourceBundle.getBundle("ua/cn/audioremote/data/"+currentLanguage).getString("about"), jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        viseble = !viseble;

    }//GEN-LAST:event_formWindowClosing

    private void jTFPortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTFPortActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTFPortActionPerformed

    private void jBOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBOpenActionPerformed
        FileDialog fd = new FileDialog(this, "Open m3u file", FileDialog.LOAD);

        fd.setFilenameFilter(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".m3u");
            }
        });
        fd.setVisible(true);
        jTFPath.setText(fd.getDirectory() + fd.getFile());
    }//GEN-LAST:event_jBOpenActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed

    private void jBSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSaveActionPerformed
        if (prop.getProperty("port").equals(jTFPort.getText().trim())) {
            ss.setPathTom3u(jTFPath.getText().trim());
            new Thread(new Runnable() {
                public void run() //Этот метод будет выполняться в побочном потоке
                {
                    ss.setListAudioInfo(ss.getPlayList(ss.getPathTom3u()));
                }
            }).start();
        } else {
            try {
                if (ss != null) {
                    ss.interrupt();
                }
                ss = new SocketServer(Integer.parseInt(jTFPort.getText().trim()), jTFPath.getText().trim());
                ss.start();
            } catch (IOException ex) {
                if (trayIcon != null) {
                    timer.schedule(new MessageDisplayTask(trayIcon, "AudioRemouteServer", "Change server port to another", TrayIcon.MessageType.ERROR), 5000);
                    // trayIcon.displayMessage("AudioRemoteServer", "Change server port to another", TrayIcon.MessageType.ERROR);
                }
            }

        }
        try {
            prop.setProperty("port", jTFPort.getText().trim());
            prop.setProperty("pathm3u", jTFPath.getText().trim());
            prop.store(new FileOutputStream("config.properties"), null);
        } catch (IOException ex) {
            Logger.getLogger(AudioRemoteServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jBSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AudioRemoteServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AudioRemoteServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AudioRemoteServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AudioRemoteServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AudioRemoteServer().setVisible(false);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBOpen;
    private javax.swing.JButton jBSave;
    private javax.swing.JLabel jLPort;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTFPath;
    private javax.swing.JTextField jTFPort;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    // End of variables declaration//GEN-END:variables

    class MessageDisplayTask extends java.util.TimerTask {

        private TrayIcon ti;
        private String title;
        private String mess;
        private TrayIcon.MessageType type;

        public MessageDisplayTask(TrayIcon ti, String title, String mess, TrayIcon.MessageType type) {
            this.ti = ti;
            this.title = title;
            this.mess = mess;
            this.type = type;
        }

        public void run() {
            ti.displayMessage(title, mess, type);
            this.cancel();
        }
    }
}
