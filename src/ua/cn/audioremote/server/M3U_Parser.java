package ua.cn.audioremote.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;

/**
 * m3u Parser
 *
 * @author Ivan Metla
 */
public class M3U_Parser {

    private LinkedList<String> url;

    /**
     * *
     *
     * @param path to m3u file
     * @throws Exception
     */
    public M3U_Parser(String path) throws Exception {
        url = new LinkedList<>();
        parseFile(new File(path));
    }

    /**
     * Convert Stream to String
     *
     * @param is
     * @return
     */
    private String convertStreamToString(java.io.InputStream is) {
        try {
            return new java.util.Scanner(is).useDelimiter("\\A").next();
        } catch (java.util.NoSuchElementException e) {
            return "";
        }
    }

    /**
     * Parse file
     *
     * @param f
     * @throws FileNotFoundException
     */
    private void parseFile(File f) throws FileNotFoundException, UnsupportedEncodingException {
        if (f.exists()) {
            String stream = convertStreamToString(new FileInputStream(f));
            stream = stream.replaceAll("#EXTM3U", "").trim();
            String[] arr = stream.split("\n");
            for (int n = 0; n < arr.length; n++) {
                if (!arr[n].contains("#EXTINF")) {
                    url.add(arr[n].trim());
                }
            }
        }
    }

    /**
     *
     * @return Collection of LinkedList<String>
     */
    public LinkedList<String> getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "M3U_Parser{" + "url=" + url + '}';
    }
    
    
}
