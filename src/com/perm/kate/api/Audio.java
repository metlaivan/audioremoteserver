package com.perm.kate.api;

import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import ua.cn.audioremote.data.OsDefinition;

public class Audio implements Serializable {

    private static final long serialVersionUID = 1L;
    public long aid;
    public long owner_id;
    public String artist;
    public String title;
    public long duration;
    public String durationInS;
    public String url;
    public Long lyrics_id;

    /**
     *
     * @param artist
     * @param title
     * @param duration in seconds
     */
    public Audio(String artist, String title, long duration) {
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.url = "";
    }

    /**
     *
     * @param url path to file
     */
    public Audio(String url) {
            try {
                URI uri = null;
                try {
                    uri = new URI("file", URLDecoder.decode(url, "UTF-8"),null);                    
                } catch (URISyntaxException ex) {
                    Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
                }               
                this.url=uri.toString();                
                Mp3File mp3file = new Mp3File(url);        
                if (mp3file.hasId3v1Tag()) {
                    this.artist = mp3file.getId3v1Tag().getArtist();
                    this.title = mp3file.getId3v1Tag().getTitle();
                } else if (mp3file.hasId3v2Tag()) {
                    this.artist = mp3file.getId3v2Tag().getArtist();
                    this.title = mp3file.getId3v2Tag().getTitle();
                } 
                if((artist==null && title==null) || (artist=="" && title=="")) {
                    char slesh;
                    if (OsDefinition.isWindows()) {
                        slesh = '\\';
                    } else {
                        slesh = '/';
                    }
                    this.title = url.substring(url.lastIndexOf(slesh) + 1, url.length() - 4);
                }
                

                this.duration = mp3file.getLengthInSeconds();
                this.durationInS = String.valueOf(duration / 60) + ":"
                        + (String.valueOf(duration % 60).length() == 1 ? ("0" + String.valueOf(duration % 60))
                        : String.valueOf(duration % 60));
            } catch (IOException ex) {
                Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedTagException ex) {
                Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidDataException ex) {
               // Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
            }
     

    }

    @Override
    public String toString() {
        return "Audio{" + "artist=" + artist + ", title=" + title + ", durationInS=" + durationInS + '}';
    }
}